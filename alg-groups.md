## Creating Algorithm Groups

1. Create function for Algorithm in `mdouleName/src/Algs_moduleName` e.g. 
```bash
//For moduleName = AC_SkinLED

void AlgsAC_SkinLED::TrackValues(std::string algName_array [], std::string histName, std::string algGroupName, float low2, float low1, float high1, float high2){
  std::string grname = histName+"GR";
  TGraph* graph = m_hists->GetScatterPlot(grname)->GetGraph(); 
  for (int k=1; k<graph->GetN(); k++){
    float algResult = graph->GetY()[k-1];
    float algError = sqrt(algResult);
    std::string algName = algName_array [k-1];
    m_PREM->AddAlgorithmGroupToJSON(histName, algGroupName, algName, algResult, algError, low2, low1, high1, high2);
  }
}
```

2. Include the new function and arguments in the `public` setion of the file in  `moduleName/include/Algs_moduleName`
```bash
class AlgsAC_SkinVUV  {

public:

    void TrackValues(std::string algName_array [], std::string histName,  std::string algGroupName, float low2, float low1, float high1, float high2);
}
```
3. Now call this function near the end of the Finalize() block of your module in `moduleName/src/moduleName.cxx`

```bash
     m_AlgsmoduleName->TrackValues(chNumbers, histName, tabName, low2, low1, high1, high2);
     m_AlgsAC_SkinLED->TrackValues(chNumbers, "SPEfDPEPlots/h_fDPEBot", "fDPEValue", low2, low1, high1, high2);
```
