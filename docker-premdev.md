# Steps to set up Docker `premdev_web`

Clone this repo:
```bash
git clone git@gitlab.com:luxzeplin/lz_services/dqm/premweb_dev.git
```
Run
```bash
docker-compose up -d --build
```
You should then be able to visit http://localhost:8080/dqm.html. However the dropdown boxes will not be populated. To populate the database. Open the docker desktop app, then mouse over the `premwebdev_node` running image, and click the `CLI` button (>_).  That should open a command line interface for this image, then type `python3 InsertStartingFiles.py`. That should run.

Refresh the site and you should be able to use the dropdown boxes. (well for now only the Run Type dropdown is populated. Working on that.

## Hit any problems? Do these steps

First make sure you dont have mongod already running.
`ps aux | grep "mongod"`
if this results in a mongod process then kill it (`kill -9 PROCESS_ID`).
