## Contents
- [Setting up CORI](#setting-up-cori)
- [Building PREM modules](#building-prem-modules)
- [Manual running of PREM](#manual-running-of-prem)
    - [Premifying the output](#premifying-the-output)
- [Requirements for automatic PREM running](#requirements-for-automatic-prem-running)
    - [runType, Runs and Modules](#runtype,-runs-and-modules)
    - [Running the script](#running-the-script)
- [Additional](#additional)
    - [Shifter setup](#shifter-setup)
    - [Setting up SSH key](#setting-up-ssh-key)

## Setting up CORI

Generate a SSH key (valid for 24hrs) to get into NERSC without the OTP (where `amardeep` should be replaced by NERSC username)

```bash
scp amardeep@cori.nersc.gov:/project/projectdirs/mfa/NERSC-MFA/sshproxy.sh .

chmod u+x sshproxy.sh
```

Use the client to generate a special file ~/.ssh/nersc:

```bash
./sshproxy.sh -u amardeep
```

Finally, you can use the client to login sans OTP:

```bash
ssh -i ~/.ssh/nersc amardeep@cori.nersc.gov
```

Find your project area (or create one if this is your first time):
```bash
cd /global/cfs/cdirs/lz/users/amardeep/
```

Clone ALPACA the same way you set it up on your remote profile following the documentation. Remember to [add an SSH key to your GitLab profile](#setting-up-ssh-key) otherwise you'll get "_Permission denied (publickey,keyboard-interactive)_" error.

## Building PREM modules

Enter shifter image (if `shifterCOS7` is unavailable, [follow these steps](#shifter-setup)).
```bash
/usr/bin/shifter --module=cvmfs --image=luxzeplin/offline_hosted:centos7 bash --rcfile ~/.bashrc.ext
```
`cd` into your ALPACA folder (`/global/cfs/cdirs/lz/users/amardeep/march21/ALPACA`) and run

```bash
source setup.sh
source build.sh
```
You can add more PREM modules by cloning them into `modules/` and running `build`. Then run the module by invoking `moduleName`.
## Manual running of PREM

### Premifying the output

The premify script requires `jq` to run, you will need to exit your shifter image, you can achieve this with the command `exit`.

An example of loading a python env (`myenv`) that contains pymongo that you have created earlier:

```bash
module load python

source activate myenv
```
You will need a python environment that has the library pymongo. It can be installed with the command `python -m pip install pymongo`.

This document details the manual method for running many modules over many Runs and then adding those modules to PREMweb.

The url of the development site is: https://lz-prem-dev.lbl.gov/.

To run the premify script, to see the results on the website:

```bash
cd run/                      #to access the run/moduleName

source ../modules/AlpacaCore/scripts/premify.sh moduleName
```



## Requirements for automatic PREM running

1. When a Run should be processed by PREM needs to be flagged.

Typically, runs that require offline analysis should be processed by PREM. In commissioning this could be calibrations like `GXe_TPC_LED_SPE`, or background runs like `GXe_BG`.

Whether the run has finished, ie has it been aborted, and how many events it contains should be considered. 

2. The runType of the Run needs to be flagged.

runTypes are used to categorize Runs and allow meaningful comparisons between Runs to be made.

Examples of runTypes during commissioning so far: `WarmGXe_PMT_Acquisitions`, `WarmGXe_SkinPMT_Acquisitions`, `GXe_BG`, `GXe_TPC_LED_SPE`, etc. 

The runType also allows us to determine which modules should be run.

3. When all RQ files have reached CORI needs to be flagged.

PREM is run over entire Runs, not individual files.


### runType, Runs and Modules
The runType and Run of your PREM module is determined by the `inputFiles/` used during this process.

The folder in `inputFiles/` corresponds to the runType. Each list file within the runType folder is a Run. Each Run file contains the location of the root files within the run.

You can select the runType you wish to use by changing the variable `runListsDirName`on line 9 of `modules/AlpacaCore/scripts/prem_automation.py`

This python script will produce an ALPACA process for each Run in that folder. If there are Runs that you do not wish to process, move them to a storage folder e.g. `inputFiles/GXe_old`.

You can select the modules you wish to run by changing the array `modules` on line 15 of the python script.

You can add new Runs and runTypes by adding folders and files to `inputFiles/`, you may wish to use a script like `source ListPROD1Files.sh 202103 GXe 2282`, the 3 arguments for that script are month, runType and Run respectively.

### Running the script

Having setup your shifter image, runType, Run and Modules, the command to the run the script is `python modules/AlpacaCore/scripts/prem_automation.py`

The temporary directory `automate_tmp` contains the log files for these processes.

The progress bar shows how many processes have finished.

The ALPACA processes have finished once the progress bar reaches 100%


## Additional 

### Shifter setup

We need `shifterCOS7` which might not be available so in order to get that create a new file 

`emacs ~/.bashrc` 

and copy the following into it:

```bash
#begin .bashrc                                                                                                                                                                                                     
if [[ -z $SHIFTER_RUNTIME ]]
then
  # Source appropriate .ext file                                                                                                                                                                                   
  SHELL_PARSING=$0
  if [ $SHELL_PARSING == -su ]; then
      SHELL_PARSING=`readlink /proc/$$/exe`
  fi
  case $SHELL_PARSING in
    -sh|sh|*/sh)
    ;;
    -ksh|ksh|*/ksh)
      if [ -e $HOME/.kshrc.ext ]; then
        typeset -xf module
        . $HOME/.kshrc.ext
      fi
    ;;
    -bash|bash|*/bash)
      if [ -e $HOME/.bashrc.ext ]; then
        . $HOME/.bashrc.ext
      fi
    ;;
  esac
fi
alias lr="ls -ltrh"
#end .bashrc     
```

Now `exit` and SSH into CORI again to check it's working. [Return back to section](#setting-up-cori).

### Setting up SSH key
Run 
```bash
ssh-keygen -t rsa -b 2048 -C
```
Then hit `Enter` three times and your key should be generated and stored in the `/global/homes/a/amardeep/.ssh/id_rsa.pub.` or equivalent.

Copy this and remove the additional `\` which indicate a new line and copy to your GitLab keys. [Return back to section](#setting-up-cori).

