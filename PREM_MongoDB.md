## Contents
- [Setup](#setup)
    - [Setting up MongoDB](#setting-up-mongodb)
    - [Setting up PyMongo](#setting-up-pymongo)
    - [Setting up NodeJS](#setting-up-nodejs)
- [Mongo Shell](#mongo-shell)
- [Mongo on MacBook](#mongo-on-macbook)

## Setup
### Setting up MongoDB
Full instructions are available [here](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/).

This needs to happen through `Homebrew` for Mac users. Once `brew` is installed, `MongoDB` can be installed:
```bash
brew install mongodb-community@4.4
```
Then activating the environment is done by:
```bash
brew services start mongodb-community@4.4

# To check it is running i.e. service mongodb-community should be listed as started
brew services list\

```
Figure out where brew saves your installed programs by running `brew --prefix` and add the Mongo path to your PATH on your `.bash_profile` or `.zprofile`. 

For me this was `/opt/homebrew/opt/mongodb-community@4.4/bin`.

Once this is done, you can creat a Mongo Shell by running:
```bash
mongo
```

### Setting up PyMongo

```bash 
pip3 install pymongo
```
### Setting up NodeJS
Download the NodeJS application from [here](https://nodejs.org/en/)
```bash 
git clone git@gitlab.com:luxzeplin/lz_services/dqm/premwebNodeJS.git

cd premwebNodeJS

# Install all the dependencies
npm install

# Install Nodemon so that any changes you make to the website can be viewed immediately
npm install -g nodemon   
``` 
If these commands return the following error, there is an access issue.
```bash
npm WARN checkPermissions Missing write access to /usr/local/lib/node_modules
npm ERR! code EACCES
npm ERR! syscall access
npm ERR! path /usr/local/lib/node_modules
npm ERR! errno -13
npm ERR! Error: EACCES: permission denied, access '/usr/local/lib/node_modules'
npm ERR!  [Error: EACCES: permission denied, access '/usr/local/lib/node_modules'] {
npm ERR!   errno: -13,
npm ERR!   code: 'EACCES',
npm ERR!   syscall: 'access',
npm ERR!   path: '/usr/local/lib/node_modules'
npm ERR! }
npm ERR! 
npm ERR! The operation was rejected by your operating system.
npm ERR! It is likely you do not have the permissions to access this file as the current user
npm ERR! 
npm ERR! If you believe this might be a permissions issue, please double-check the
npm ERR! permissions of the file and its containing directories, or try running
npm ERR! the command again as root/Administrator.
```
This is resolved by running 
```bash
sudo chown -R $USER /usr/local/lib/node_modules
```
## Mongo Shell
Open the mongo shell using the command `mongo` as before.

We need to create and admin user and a database called `admin`. The `user` and `pwd` can be changed as you wish but will need to be consistent throughout the rest of this setup.

```bash
use admin
```
```bash
db.createUser(
  {
    user: "user",
    pwd: "password",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
```
Exit the mongo shell using `exit`.

## Mongo on MacBook
If developing on a MacBook the `mongo` of choice is the community version installed via `Brew`.
If the connection dies, seen a few times so far, you would first notice the web page not populate any drop down boxes or the history plots.

To fix this restart the `mongodb`. (If deleting the mongodb and adding in the data again manually then please ensure you have added the user which aligns with the username and password in the env.sh file.)
```bash
sudo brew services stop mongodb-community
```
```bash
sudo brew services start mongodb-community
```

NOTE: `sudo brew` gives a warning but this is the only way I have found to fix this.
