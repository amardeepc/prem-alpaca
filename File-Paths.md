## Runs
- [LED Data](#led-data)
    - [Top Paths](#top-paths)
    - [Bot Paths](#bot-paths)
    - [Dome Paths](#dome-paths)
- [VUV Data](#vuv-data)
    - [Top Paths](#top-paths)
    - [Bot Paths](#bot-paths)
    - [Dome Paths](#dome-paths)

## LED Data
### Top Paths
T5 - Run 2157:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103010432_002157
```
T6 - Run 2269:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103050217_002269
```
T7 - Run 2277:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103050632_002277
```
### Bot Paths
Run 2940:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103241209_002940
```
Run 2941:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103241221_002941
```
Run 2942:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103241232_002942
```
### Dome Paths
Run 2931:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103240938_002931
```
Run 2932:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103240948_002932
```
Run 2933: Not transferred

## VUV Data
### Top Paths(#t1)
None

### Bot Paths
Run 2936:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103241048_002936
```
Run 2939:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103241131_002939
```
### Dome Paths
Run 2929:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103240836_002929
```
Run 2930:
```bash
/lz/data/reconstructed/commissioning/LZAP-5.2.5_PROD-0/202103/lz_202103240854_002930
```
